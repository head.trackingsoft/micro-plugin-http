<?php

namespace Micro\Plugin\Http\Configuration\Context;

use Micro\Framework\Kernel\Configuration\PluginRoutingKeyConfiguration;

class KernelContextConfiguration extends PluginRoutingKeyConfiguration implements KernelContextConfigurationInterface
{

    protected const CFG_BASE_URL   = 'HTTP_%s_BASE_URL';
    protected const CFG_PATH_INFO  = 'HTTP_%s_PATH_INFO';
    protected const CFG_HOST       = 'HTTP_%s_HOST';
    protected const CFG_SCHEME     = 'HTTP_%s_SCHEME';
    protected const CFG_HTTP_PORT  = 'HTTP_%s_HTTP_PORT';
    protected const CFG_HTTPS_PORT = 'HTTP_%s_HTTPS_PORT';

    /**
     * {@inheritDoc}
     */
    public function getBaseUrl(): string
    {
        return $this->get(self::CFG_BASE_URL);
    }

    /**
     * {@inheritDoc}
     */
    public function getPathInfo(): string
    {
        return $this->get(self::CFG_PATH_INFO);
    }

    /**
     * {@inheritDoc}
     */
    public function getHost(): string
    {
        return $this->get(self::CFG_HOST, self::HOST_DEFAULT);
    }

    /**
     * {@inheritDoc}
     */
    public function getScheme(): string
    {
        return $this->get(self::CFG_SCHEME, self::SCHEME_DEFAULT);
    }

    /**
     * {@inheritDoc}
     */
    public function getHttpPort(): int
    {
        return (int)$this->get(self::CFG_HTTP_PORT, self::HTTP_PORT_DEFAULT);
    }

    /**
     * {@inheritDoc}
     */
    public function getHttpsPort(): int
    {
        return (int)$this->get(self::CFG_HTTPS_PORT, self::HTTPS_PORT_DEFAULT);
    }
}
