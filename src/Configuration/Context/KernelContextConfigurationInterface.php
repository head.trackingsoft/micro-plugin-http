<?php

namespace Micro\Plugin\Http\Configuration\Context;

interface KernelContextConfigurationInterface
{
    public const HTTP_PORT_DEFAULT  = 80;
    public const HTTPS_PORT_DEFAULT = 443;
    public const SCHEME_DEFAULT     = 'http';
    public const HOST_DEFAULT       = 'localhost';

    /**
     * @return string
     */
    public function getBaseUrl(): string;

    /**
     * @return string
     */
    public function getPathInfo(): string;

    /**
     * @return string
     */
    public function getHost(): string;

    /**
     * @return string
     */
    public function getScheme(): string;

    /**
     * @return int
     */
    public function getHttpPort(): int;

    /**
     * @return int
     */
    public function getHttpsPort(): int;
}
