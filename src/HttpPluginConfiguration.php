<?php

namespace Micro\Plugin\Http;

use Micro\Framework\Kernel\Configuration\PluginConfiguration;
use Micro\Plugin\Http\Configuration\Context\KernelContextConfiguration;

class HttpPluginConfiguration extends PluginConfiguration implements HttpPluginConfigurationInterface
{
    /*
     * @param string $kernelAlias
     *
     * @return KernelContextConfiguration
     */
    //    public function getKernelContextConfiguration(string $kernelAlias = HttpPluginConfigurationInterface::HTTP_KERNEL_DEFAULT)
    //    {
    //        return new KernelContextConfiguration($this->configuration, $kernelAlias);
    //    }
}
