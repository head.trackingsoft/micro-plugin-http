<?php

namespace Micro\Plugin\Http;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface HttpFacadeInterface
{
    /**
     * @param Request $request
     *
     * @throws \Throwable
     *
     * @return Response
     */
    public function handle(Request $request): Response;
}
