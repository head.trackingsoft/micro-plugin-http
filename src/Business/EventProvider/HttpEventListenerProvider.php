<?php

namespace Micro\Plugin\Http\Business\EventProvider;

use Micro\Component\EventEmitter\Impl\Provider\AbstractListenerProvider;
use Micro\Plugin\Http\Business\Listener\ApplicationStartListener;
use Micro\Plugin\Http\Business\Request\RequestFactoryInterface;
use Micro\Plugin\Http\HttpFacadeInterface;

class HttpEventListenerProvider extends AbstractListenerProvider
{
    /**
     * @param HttpFacadeInterface $httpFacade
     * @param RequestFactoryInterface $requestFactory
     */
    public function __construct(
    private HttpFacadeInterface $httpFacade,
    private RequestFactoryInterface $requestFactory
    )
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getEventListeners(): iterable
    {
        return [
            $this->createApplicationStartListener()
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return 'http';
    }

    protected function createApplicationStartListener()
    {
        return new ApplicationStartListener(
            $this->httpFacade,
            $this->requestFactory
        );
    }
}
