<?php

namespace Micro\Plugin\Http\Business\EventDispatcher;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

interface EventDispatcherFactoryInterface
{
    /**
     * @param Request $request
     * @return EventDispatcherInterface
     */
    public function create(Request $request): EventDispatcherInterface;
}
