<?php

namespace Micro\Plugin\Http\Business\EventDispatcher;

use Micro\Plugin\EventEmitter\EventsFacadeInterface;
use Micro\Plugin\Http\Business\Event\ControllerArgumentsEvent;
use Micro\Plugin\Http\Business\Event\ControllerEvent;
use Micro\Plugin\Http\Business\Event\ExceptionEvent;
use Micro\Plugin\Http\Business\Event\FinishRequestEvent;
use Micro\Plugin\Http\Business\Event\RequestEvent;
use Micro\Plugin\Http\Business\Event\ResponseEvent;
use Micro\Plugin\Http\Business\Event\TerminateEvent;
use Micro\Plugin\Http\Business\Event\ViewEvent;
use Micro\Plugin\Http\Business\Request\RequestContextFactoryInterface;
use Micro\Plugin\Http\Business\Router\RouteCollectionFactoryInterface;
use Micro\Plugin\Http\Business\Router\RouteExtractorFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherFactoryInterface as SymfonyEventDispatcherFactoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherFactory as SymfonyEventDispatcherFactory;

class EventDispatcherFactory implements EventDispatcherFactoryInterface
{
    /**
     * Symfony to Micro mapping
     */
    protected const EVENT_MAPPING = [
        KernelEvents::REQUEST => RequestEvent::class,
        KernelEvents::CONTROLLER => ControllerEvent::class,
        KernelEvents::CONTROLLER_ARGUMENTS => ControllerArgumentsEvent::class,
        KernelEvents::VIEW  => ViewEvent::class,
        KernelEvents::RESPONSE  => ResponseEvent::class,
        KernelEvents::FINISH_REQUEST => FinishRequestEvent::class,
        KernelEvents::TERMINATE => TerminateEvent::class,
        KernelEvents::EXCEPTION => ExceptionEvent::class,
    ];

    /**
     * @param EventsFacadeInterface $eventsFacade
     * @param RequestContextFactoryInterface $requestContextFactory
     * @param RouteCollectionFactoryInterface $routeExtractorFactory
     */
    public function __construct(
    private EventsFacadeInterface $eventsFacade,
    private RequestContextFactoryInterface $requestContextFactory,
    private RouteCollectionFactoryInterface $routeCollectionFactory
    )
    {
    }

    /**
     * {@inheritDoc}
     */
    public function create(Request $request): SymfonyEventDispatcherFactoryInterface
    {
        $symfonyEventDispatcher =  new SymfonyEventDispatcherFactory();
        $symfonyEventDispatcher->addSubscriber(
            $this->createRouterListener($request)
        );

        $this->proxyEvents($symfonyEventDispatcher);

        return $symfonyEventDispatcher;
    }

    /**
     * @return RouterListener
     */
    protected function createRouterListener(Request $request): RouterListener
    {
        return new RouterListener(
            $this->createUrlMatcher($request),
            $this->createRequestStack()
        );
    }

    /**
     * @param Request $request
     *
     * @return UrlMatcherInterface
     */
    protected function createUrlMatcher(Request $request): UrlMatcherInterface
    {
        $routeCollection = $this->routeCollectionFactory->create();
        $requestContext  = $this->requestContextFactory->create($request);

        return new UrlMatcher($routeCollection, $requestContext);
    }

    /**
     * @return RequestStack
     */
    protected function createRequestStack(): RequestStack
    {
        return new RequestStack();
    }

    /**
     * @param SymfonyEventDispatcherFactoryInterface $eventDispatcher
     * @return void
     *
     *@todo: Temporary solution
     *
     */
    protected function proxyEvents(SymfonyEventDispatcherFactoryInterface $eventDispatcher): void
    {
        foreach (self::EVENT_MAPPING as $symfonyEventName => $microEventClass) {
            $eventDispatcher->addListener($symfonyEventName, function (Event $event) use ($microEventClass) {
                $this->eventsFacade->emit(new $microEventClass($event));
            });
        }
    }
}
