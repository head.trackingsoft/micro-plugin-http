<?php

namespace Micro\Plugin\Http\Business\Argument;

use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;

class ArgumentResolverFactory implements ArgumentResolverFactoryInterface
{
    /**
     * @return ArgumentResolverInterface
     */
    public function create(): ArgumentResolverInterface
    {
        return new ArgumentResolver();
    }
}
