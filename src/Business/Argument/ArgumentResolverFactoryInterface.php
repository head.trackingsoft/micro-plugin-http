<?php

namespace Micro\Plugin\Http\Business\Argument;

use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;

interface ArgumentResolverFactoryInterface
{
    /**
     * @return ArgumentResolverInterface
     */
    public function create(): ArgumentResolverInterface;
}
