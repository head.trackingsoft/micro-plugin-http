<?php

namespace Micro\Plugin\Http\Business\Controller;

use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;

interface ControllerResolverFactoryInterface
{
    /**
     * @return ControllerResolverInterface
     */
    public function create(): ControllerResolverInterface;
}
