<?php

namespace Micro\Plugin\Http\Business\Controller;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;

class ControllerResolverFactory implements ControllerResolverFactoryInterface
{
    public function __construct(private LoggerInterface $logger = new NullLogger())
    {
    }

    /**
     * {@inheritDoc}
     */
    public function create(): ControllerResolverInterface
    {
        return new ControllerResolver($this->logger);
    }
}
