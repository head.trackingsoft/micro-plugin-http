<?php

namespace Micro\Plugin\Http\Business\Router;

use Micro\Kernel\App\AppKernelInterface;

class RouteExtractorFactory implements RouteExtractorFactoryInterface
{
    /**
     * @param AppKernelInterface $appKernel
     */
    public function __construct(private AppKernelInterface $appKernel)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function create(): RouteExtractorInterface
    {
        return new RouteFromApplicationExtractor($this->appKernel);
    }
}
