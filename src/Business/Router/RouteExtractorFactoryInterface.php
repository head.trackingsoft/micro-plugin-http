<?php

namespace Micro\Plugin\Http\Business\Router;

interface RouteExtractorFactoryInterface
{
    /**
     * @return RouteExtractorInterface
     */
    public function create(): RouteExtractorInterface;
}
