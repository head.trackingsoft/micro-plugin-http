<?php

namespace Micro\Plugin\Http\Business\Router;

use Micro\Plugin\Http\HttpPluginConfigurationInterface;

abstract class AbstractRouteProvider implements RouterProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function getHttpKernelName(): string
    {
        return HttpPluginConfigurationInterface::HTTP_KERNEL_DEFAULT;
    }

    /**
     * @return iterable<Route>
     */
    abstract public function getRouteCollection(): iterable;
}
