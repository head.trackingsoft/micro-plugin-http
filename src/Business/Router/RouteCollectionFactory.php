<?php

namespace Micro\Plugin\Http\Business\Router;

use Micro\Plugin\Http\HttpPluginConfigurationInterface;
use Symfony\Component\Routing\RouteCollection;

class RouteCollectionFactory implements RouteCollectionFactoryInterface
{
    /**
     * @param RouteExtractorFactoryInterface $routeExtractorFactory
     */
    public function __construct(private RouteExtractorFactoryInterface $routeExtractorFactory)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function create(string $kernelAlias = HttpPluginConfigurationInterface::HTTP_KERNEL_DEFAULT): RouteCollection
    {
        $routeCollection = new RouteCollection();
        $routeExtractor  = $this->routeExtractorFactory->create();
        foreach ($routeExtractor->extract($kernelAlias) as $route) {
            $routeCollection->add($route->getName(), $route);
        }

        return $routeCollection;
    }
}
