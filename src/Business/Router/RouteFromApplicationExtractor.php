<?php

namespace Micro\Plugin\Http\Business\Router;

use Micro\Kernel\App\AppKernelInterface;
use Micro\Plugin\Http\Business\Plugin\RouteProviderPluginInterface;

class RouteFromApplicationExtractor implements RouteExtractorInterface
{
    /**
     * @param AppKernelInterface $appKernel
     */
    public function __construct(private AppKernelInterface $appKernel)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function extract(string $kernelAlias): iterable
    {
        foreach ($this->extractRouteProviderCollection($kernelAlias) as $collection) {
            yield $collection;
        }
    }

    /**
     * @return iterable<Route>
     */
    protected function extractRouteProviderCollection(string $kernelAlias): iterable
    {
        foreach ($this->appKernel->plugins() as $plugin) {
            if(!($plugin instanceof RouteProviderPluginInterface)) {
                continue;
            }

            foreach ($plugin->getRouteProviders() as $provider) {
                if($provider->getHttpKernelName() !== $kernelAlias) {
                    continue;
                }

                foreach ($provider->getRouteCollection() as $route) {
                    yield $route;
                }
            }
        }

        return null;
    }
}
