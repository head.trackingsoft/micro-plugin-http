<?php

namespace Micro\Plugin\Http\Business\Router;

use Symfony\Component\Routing\Route as SymfonyRoute;

class Route extends SymfonyRoute
{

    public function __construct(
        string $path,
    private ?string $name = null,
        array $defaults = [],
        array $requirements = [],
        array $options = [],
        ?string $host = '',
        array|string $schemes = [],
        array|string $methods = [],
        ?string $condition = ''
    )
    {
        parent::__construct($path, $defaults, $requirements, $options, $host, $schemes, $methods, $condition);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name ?? $this->getPath();
    }
}
