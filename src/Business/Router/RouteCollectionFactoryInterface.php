<?php

namespace Micro\Plugin\Http\Business\Router;

use Micro\Plugin\Http\HttpPluginConfigurationInterface;
use Symfony\Component\Routing\RouteCollection;

interface RouteCollectionFactoryInterface
{
    /**
     * @param string $kernelAlias
     *
     * @return RouteCollection
     */
    public function create(string $kernelAlias = HttpPluginConfigurationInterface::HTTP_KERNEL_DEFAULT): RouteCollection;
}
