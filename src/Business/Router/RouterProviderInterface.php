<?php

namespace Micro\Plugin\Http\Business\Router;

interface RouterProviderInterface
{
    /**
     * @return string
     */
    public function getHttpKernelName(): string;

    /**
     * @return iterable<Route>
     */
    public function getRouteCollection(): iterable;
}
