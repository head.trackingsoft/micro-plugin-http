<?php

namespace Micro\Plugin\Http\Business\Router;

interface RouteExtractorInterface
{
    /**
     * @param string $kernelAlias
     *
     * @return iterable<Route>
     */
    public function extract(string $kernelAlias): iterable;
}
