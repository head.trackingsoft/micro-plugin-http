<?php

namespace Micro\Plugin\Http\Business\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;

interface RequestContextFactoryInterface
{
    /**
     * @param Request|null $request
     *
     * @return RequestContext
     */
    public function create(Request $request = null): RequestContext;
}
