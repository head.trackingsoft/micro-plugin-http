<?php

namespace Micro\Plugin\Http\Business\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;

class RequestContextFactory implements RequestContextFactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function create(Request $request = null): RequestContext
    {
        $requestContext =  new RequestContext();

        if($request !== null) {
            $requestContext->fromRequest($request);
        }

        return $requestContext;
    }
}
