<?php

namespace Micro\Plugin\Http\Business\Request;

use Symfony\Component\HttpFoundation\Request;

interface RequestFactoryInterface
{
    /**
     *
     * @see \Symfony\Component\HttpFoundation\Request::__construct
     *
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param $content
     *
     * @return Request
     */
    public function create(
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ): Request;
}
