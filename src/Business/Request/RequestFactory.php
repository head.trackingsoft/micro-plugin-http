<?php

namespace Micro\Plugin\Http\Business\Request;

use Symfony\Component\HttpFoundation\Request;

class RequestFactory implements RequestFactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function create(
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ): Request
    {
        if($content === null 
            && count($query) === 0 
            && count($request) === 0 
            && count($attributes) === 0 
            && count($cookies) === 0 
            && count($files) === 0 
            && count($server) === 0
        ) {
            return Request::createFromGlobals();
        }

        return new Request($query, $request, $attributes, $cookies, $files, $server, $content);
    }
}
