<?php

namespace Micro\Plugin\Http\Business\Event;

use Symfony\Component\HttpKernel\Event\KernelEvent;

class AbstractHttpEvent
{
    /**
     * @param KernelEvent $kernelEvent
     */
    public function __construct(protected KernelEvent $kernelEvent)
    {
    }

    /**
     * @param string $name
     *
     * @param array $arguments
     *
     * @return void
     */
    public function __call(string $name, array $arguments)
    {
        $this->kernelEvent->{$name}(...$arguments);
    }
}
