<?php

namespace Micro\Plugin\Http\Business\Event;

use Micro\Component\EventEmitter\EventInterface;

/**
 * @method \Throwable getThrowable()
 * @method void setThrowable(\Throwable $exception)
 * @method void allowCustomResponseCode()
 * @method bool isAllowingCustomResponseCode()
 */
class ExceptionEvent extends AbstractHttpEvent implements EventInterface
{
}
