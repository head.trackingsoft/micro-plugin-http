<?php

namespace Micro\Plugin\Http\Business\Event;

use Micro\Component\EventEmitter\EventInterface;

/**
 * @method callable getController()
 * @method void  setController(callable $controller)
 */
class ControllerEvent extends AbstractHttpEvent implements EventInterface
{
}
