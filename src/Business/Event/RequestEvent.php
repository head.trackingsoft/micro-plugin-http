<?php

namespace Micro\Plugin\Http\Business\Event;

use Micro\Component\EventEmitter\EventInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * @method Response|null getResponse()
 * @method void setResponse(Response $response)
 * @method bool hasResponse()
 */
class RequestEvent extends AbstractHttpEvent implements EventInterface
{
}
