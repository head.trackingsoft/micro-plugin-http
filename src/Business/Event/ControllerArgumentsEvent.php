<?php

namespace Micro\Plugin\Http\Business\Event;

use Micro\Component\EventEmitter\EventInterface;

/**
 * @method callable getController()
 * @method void setController(callable $controller)
 * @method array getArguments()
 * @method void setArguments(array $arguments)
 */
class ControllerArgumentsEvent extends AbstractHttpEvent implements EventInterface
{
}
