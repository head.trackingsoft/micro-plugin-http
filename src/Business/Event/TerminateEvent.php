<?php

namespace Micro\Plugin\Http\Business\Event;

use Micro\Component\EventEmitter\EventInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * @method Response getResponse()
 */
class TerminateEvent extends AbstractHttpEvent implements EventInterface
{
}
