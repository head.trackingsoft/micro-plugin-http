<?php

namespace Micro\Plugin\Http\Business\Event;

use Micro\Component\EventEmitter\EventInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * @method Response getResponse()
 * @method void setResponse(Response $response)
 */
class ResponseEvent extends AbstractHttpEvent implements EventInterface
{
}
