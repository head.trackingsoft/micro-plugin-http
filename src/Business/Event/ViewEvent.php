<?php

namespace Micro\Plugin\Http\Business\Event;

use Micro\Component\EventEmitter\EventInterface;

/**
 * @method mixed  getControllerResult()
 * @method void setControllerResult(mixed $controllerResult)
 */
class ViewEvent extends AbstractHttpEvent implements EventInterface
{
}
