<?php

namespace Micro\Plugin\Http\Business\Event;

use Micro\Component\EventEmitter\EventInterface;

class FinishRequestEvent extends AbstractHttpEvent implements EventInterface
{
}
