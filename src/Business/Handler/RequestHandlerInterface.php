<?php

namespace Micro\Plugin\Http\Business\Handler;

use Symfony\Component\HttpFoundation\Response;

interface RequestHandlerInterface
{
    /**
     * @throws \Exception When an Exception occurs during processing
     *
     * @return Response
     */
    public function handle(): Response;
}
