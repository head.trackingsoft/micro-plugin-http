<?php

namespace Micro\Plugin\Http\Business\Handler;

use Micro\Plugin\Http\Business\Kernel\HttpKernelManagerInterface;
use Micro\Plugin\Http\HttpPluginConfigurationInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RequestHandler implements RequestHandlerInterface
{
    /**
     * @param HttpKernelManagerInterface $kernelManager
     * @param Request $request
     * @param int $type
     */
    public function __construct(
    private HttpKernelManagerInterface $kernelManager,
    private Request $request,
    private int $type
    )
    {
    }

    /**
     * {@inheritDoc}
     */
    public function handle(string $kernelAlias = HttpPluginConfigurationInterface::HTTP_KERNEL_DEFAULT): Response
    {
        $httpKernel = $this->kernelManager->lookup($this->request, $kernelAlias);
        $response   = $httpKernel->handle($this->request, $this->type);
        $response->send();

        return $response;
    }
}
