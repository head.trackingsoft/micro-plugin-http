<?php

namespace Micro\Plugin\Http\Business\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

interface RequestHandlerFactoryInterface
{
    /**
     * @param Request $request
     * @param int $type
     *
     * @return RequestHandlerInterface
     */
    public function create(Request $request, int $type = HttpKernelInterface::MAIN_REQUEST): RequestHandlerInterface;
}
