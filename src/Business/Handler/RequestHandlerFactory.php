<?php

namespace Micro\Plugin\Http\Business\Handler;

use Micro\Plugin\Http\Business\Kernel\HttpKernelManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class RequestHandlerFactory implements RequestHandlerFactoryInterface
{
    /**
     * @param HttpKernelManagerInterface $httpKernelManager
     */
    public function __construct(private HttpKernelManagerInterface $httpKernelManager)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function create(Request $request, int $type = HttpKernelInterface::MAIN_REQUEST): RequestHandlerInterface
    {
        return new RequestHandler(
            $this->httpKernelManager,
            $request,
            $type
        );
    }
}
