<?php

namespace Micro\Plugin\Http\Business\Listener;

use Micro\Component\EventEmitter\EventInterface;
use Micro\Component\EventEmitter\EventListenerInterface;
use Micro\Kernel\App\AppKernelInterface;
use Micro\Kernel\App\Business\Event\ApplicationReadyEvent;
use Micro\Plugin\Http\Business\Request\RequestFactoryInterface;
use Micro\Plugin\Http\HttpFacadeInterface;
use Micro\Plugin\Http\HttpProviderInterface;

class ApplicationStartListener implements EventListenerInterface
{
    /**
     * @param HttpFacadeInterface $httpFacade
     * @param RequestFactoryInterface $requestFactory
     */
    public function __construct(
    private HttpFacadeInterface $httpFacade,
    private RequestFactoryInterface $requestFactory
    )
    {
    }

    /**
     * @param ApplicationReadyEvent $event
     *
     * @return void
     */
    public function on(EventInterface $event): void
    {
        if(!$this->isHttp()) {
            return;
        }

        $this->httpFacade->handle($this->requestFactory->create());
    }

    /**
     * @return bool
     */
    protected function isHttp(): bool
    {
        return PHP_SAPI !== 'cli';
    }

    /**
     * {@inheritDoc}
     */
    public function supports(EventInterface $event): bool
    {
        return $event instanceof ApplicationReadyEvent;
    }
}
