<?php

namespace Micro\Plugin\Http\Business\Kernel;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class HttpKernelManager implements HttpKernelManagerInterface
{
    /**
     * @var HttpKernelInterface[]
     */
    private array $kernelCollection;

    /**
     * @param HttpKernelFactoryInterface $httpKernelFactory
     */
    public function __construct(private HttpKernelFactoryInterface $httpKernelFactory)
    {
        $this->kernelCollection = [];
    }

    /**
     * {@inheritDoc}
     */
    public function lookup(Request $request, string $kernelAlias): HttpKernelInterface
    {
        if(!array_key_exists($kernelAlias, $this->kernelCollection)) {
            $this->kernelCollection[$kernelAlias] = $this->httpKernelFactory->create($request);
        }

        return $this->kernelCollection[$kernelAlias];
    }
}
