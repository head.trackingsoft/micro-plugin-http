<?php

namespace Micro\Plugin\Http\Business\Kernel;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

interface HttpKernelFactoryInterface
{
    /**
     * @param Request $request
     *
     * @return HttpKernelInterface
     */
    public function create(Request $request): HttpKernelInterface;
}
