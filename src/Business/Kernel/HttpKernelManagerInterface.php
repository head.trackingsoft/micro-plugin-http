<?php

namespace Micro\Plugin\Http\Business\Kernel;

use Micro\Plugin\Http\Exception\HttpKernelAlreadyRegisteredException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;

interface HttpKernelManagerInterface
{
    /**
     * @param Request $request
     * @param string $kernelAlias
     *
     * @throws HttpKernelAlreadyRegisteredException
     *
     * @return HttpKernel
     */
    public function lookup(Request $request, string $kernelAlias): HttpKernelInterface;
}
