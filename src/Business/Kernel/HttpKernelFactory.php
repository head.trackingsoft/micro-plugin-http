<?php

namespace Micro\Plugin\Http\Business\Kernel;

use Micro\Plugin\Http\Business\Controller\ControllerResolverFactoryInterface;
use Micro\Plugin\Http\Business\EventDispatcher\EventDispatcherFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class HttpKernelFactory implements HttpKernelFactoryInterface
{
    /**
     * @param ControllerResolverFactoryInterface $controllerResolverFactory
     * @param EventDispatcherFactoryInterface $eventDispatcherFactory
     */
    public function __construct(
    private ControllerResolverFactoryInterface $controllerResolverFactory,
    private EventDispatcherFactoryInterface $eventDispatcherFactory
    )
    {
    }

    /**
     * {@inheritDoc}
     */
    public function create(Request $request): HttpKernelInterface
    {
        return new HttpKernel(
            $this->eventDispatcherFactory->create($request),
            $this->controllerResolverFactory->create(),
            new RequestStack(),
            new ArgumentResolver()
        );
    }
}
