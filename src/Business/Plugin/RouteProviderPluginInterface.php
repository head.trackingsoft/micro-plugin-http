<?php

namespace Micro\Plugin\Http\Business\Plugin;

use Micro\Plugin\Http\Business\Router\RouterProviderInterface;

interface RouteProviderPluginInterface
{
    /**
     * @return iterable<RouterProviderInterface>
     */
    public function getRouteProviders(): iterable;
}
