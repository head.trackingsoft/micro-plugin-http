<?php

namespace Micro\Plugin\Http;

use Micro\Component\DependencyInjection\Container;
use Micro\Component\EventEmitter\ListenerProviderInterface;
use Micro\Framework\Kernel\Plugin\AbstractPlugin;
use Micro\Kernel\App\AppKernelInterface;
use Micro\Kernel\App\Business\ApplicationListenerProviderPluginInterface;
use Micro\Plugin\EventEmitter\EventsFacadeInterface;
use Micro\Plugin\Http\Business\Controller\ControllerResolverFactory;
use Micro\Plugin\Http\Business\Controller\ControllerResolverFactoryInterface;
use Micro\Plugin\Http\Business\EventDispatcher\EventDispatcherFactory;
use Micro\Plugin\Http\Business\EventDispatcher\EventDispatcherFactoryInterface;
use Micro\Plugin\Http\Business\EventProvider\HttpEventListenerProvider;
use Micro\Plugin\Http\Business\Handler\RequestHandlerFactory;
use Micro\Plugin\Http\Business\Handler\RequestHandlerFactoryInterface;
use Micro\Plugin\Http\Business\Kernel\HttpKernelFactory;
use Micro\Plugin\Http\Business\Kernel\HttpKernelFactoryInterface;
use Micro\Plugin\Http\Business\Kernel\HttpKernelManager;
use Micro\Plugin\Http\Business\Kernel\HttpKernelManagerInterface;
use Micro\Plugin\Http\Business\Request\RequestContextFactory;
use Micro\Plugin\Http\Business\Request\RequestContextFactoryInterface;
use Micro\Plugin\Http\Business\Request\RequestFactory;
use Micro\Plugin\Http\Business\Request\RequestFactoryInterface;
use Micro\Plugin\Http\Business\Router\RouteCollectionFactory;
use Micro\Plugin\Http\Business\Router\RouteCollectionFactoryInterface;
use Micro\Plugin\Http\Business\Router\RouteExtractorFactory;
use Micro\Plugin\Http\Business\Router\RouteExtractorFactoryInterface;

class HttpPlugin extends AbstractPlugin implements ApplicationListenerProviderPluginInterface
{
    /**
     * @var Container
     */
    protected Container $container;

    /**
     * @var HttpKernelManagerInterface|null
     */
    private ?HttpKernelManagerInterface $httpKernelManager = null;

    /**
     * {@inheritDoc}
     */
    public function provideDependencies(Container $container): void
    {
        $this->container = $container;

        $container->register(HttpFacadeInterface::class, function (Container $container) {
            return $this->createHttpFacade();
        });
    }

    /**
     * @return HttpFacadeInterface
     */
    protected function createHttpFacade(): HttpFacadeInterface
    {
        return new HttpFacade(
            $this->createRequestHandlerFactory()
        );
    }

    /**
     * @return RequestFactoryInterface
     */
    protected function createRequestFactory(): RequestFactoryInterface
    {
        return new RequestFactory();
    }

    /**
     * @return HttpKernelManagerInterface
     */
    protected function createKernelManager(): HttpKernelManagerInterface
    {
        if($this->httpKernelManager === null) {
            $this->httpKernelManager = new HttpKernelManager(
                $this->createHttpKernelFactory()
            );
        }

        return $this->httpKernelManager;
    }

    /**
     * @return RequestContextFactoryInterface
     */
    protected function createRequestContextFactory(): RequestContextFactoryInterface
    {
        return new RequestContextFactory();
    }

    /**
     * @return ControllerResolverFactoryInterface
     */
    protected function createControllerResolverFactory(): ControllerResolverFactoryInterface
    {
        return new ControllerResolverFactory();
    }

    /**
     * @return HttpKernelFactoryInterface
     */
    protected function createHttpKernelFactory(): HttpKernelFactoryInterface
    {
        return new HttpKernelFactory(
            $this->createControllerResolverFactory(),
            $this->createEventDispatcherFactory()
        );
    }

    /**
     * @return RouteExtractorFactoryInterface
     */
    protected function createRouteExtractorFactory(): RouteExtractorFactoryInterface
    {
        return new RouteExtractorFactory($this->lookupAppKernel());
    }

    /**
     * @return EventDispatcherFactoryInterface
     */
    protected function createEventDispatcherFactory(): EventDispatcherFactoryInterface
    {
        return new EventDispatcherFactory(
            $this->lookupEventsFacade(),
            $this->createRequestContextFactory(),
            $this->createRouteCollectionFactory()
        );
    }

    /**
     * @return RouteCollectionFactoryInterface
     */
    protected function createRouteCollectionFactory(): RouteCollectionFactoryInterface
    {
        return new RouteCollectionFactory($this->createRouteExtractorFactory());
    }

    /**
     * @return RequestHandlerFactoryInterface
     */
    protected function createRequestHandlerFactory(): RequestHandlerFactoryInterface
    {
        return new RequestHandlerFactory($this->createKernelManager());
    }

    /**
     * @return AppKernelInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function lookupAppKernel(): AppKernelInterface
    {
        return $this->container->get(AppKernelInterface::class);
    }

    /**
     * @return HttpFacadeInterface
     */
    protected function lookupHttpFacade(): HttpFacadeInterface
    {
        return $this->container->get(HttpFacadeInterface::class);
    }

    /**
     * @return EventsFacadeInterface
     */
    protected function lookupEventsFacade(): EventsFacadeInterface
    {
        return $this->container->get(EventsFacadeInterface::class);
    }

    /**
     * @return ListenerProviderInterface
     */
    public function getEventListenerProvider(): ListenerProviderInterface
    {
        return new HttpEventListenerProvider(
            $this->lookupHttpFacade(),
            $this->createRequestFactory()
        );
    }
}
