<?php

namespace Micro\Plugin\Http\Exception;

class HttpKernelNotFoundException extends RuntimeException
{
    /**
     * @param string $kernelName
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(string $kernelName = "", int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct(sprintf('Http Kernel not found by alias "%s"', $kernelName), $code, $previous);
    }
}
