<?php

namespace Micro\Plugin\Http\Exception;

class HttpKernelAlreadyRegisteredException extends RuntimeException
{
    /**
     * @param string $kernelName
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(string $kernelName = "", int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct(sprintf('Http Kernel with alias "%s" already registered.', $kernelName), $code, $previous);
    }
}
