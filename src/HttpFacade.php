<?php

namespace Micro\Plugin\Http;

use Micro\Plugin\Http\Business\Handler\RequestHandlerFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HttpFacade implements HttpFacadeInterface
{
    /**
     * @param RequestHandlerFactoryInterface $requestHandlerFactory
     */
    public function __construct(private RequestHandlerFactoryInterface $requestHandlerFactory)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function handle(Request $request): Response
    {
        return $this->requestHandlerFactory
            ->create($request)
            ->handle();
    }
}
