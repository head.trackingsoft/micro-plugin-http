<?php

namespace Micro\Plugin\Http;

interface HttpPluginConfigurationInterface
{
    public const HTTP_KERNEL_DEFAULT = 'http.kernel.default';

    /*
     * @param string $kernelAlias
     * @return mixed
     */
    //    public function getKernelContextConfiguration(string $kernelAlias = self::HTTP_KERNEL_DEFAULT);
}
